package com.avawoman.test

import androidx.test.espresso.intent.rule.IntentsTestRule
import androidx.test.filters.MediumTest
import androidx.test.runner.AndroidJUnit4
import com.avawoman.test.domain.Repository
import com.avawoman.test.util.TestApp
import com.avawoman.test.view.main.MainActivity
import org.junit.Assert
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import javax.inject.Inject

@RunWith(AndroidJUnit4::class)
@MediumTest
open class DiTest {

    @Inject
    lateinit var repository: Repository

    @JvmField
    @Rule
    var mActivityRule: IntentsTestRule<MainActivity> = IntentsTestRule(MainActivity::class.java)

    @Before
    fun setUp() {
        TestApp.appComponent.inject(this)
    }

    @Test
    fun isDependencyInjected() {
        Assert.assertNotNull(repository)
    }
}
