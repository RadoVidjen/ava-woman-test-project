package com.avawoman.test.util

import androidx.test.InstrumentationRegistry
import com.avawoman.test.App
import com.avawoman.test.injection.components.DaggerMockApplicationComponent
import com.avawoman.test.injection.components.MockApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication

class TestApp : App() {

    lateinit var appComponent: MockApplicationComponent internal set

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        appComponent = DaggerMockApplicationComponent
                .builder()
                .application(this)
                .build()

        return appComponent
    }

    companion object {
        val appComponent: MockApplicationComponent
            get() = (InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as TestApp).appComponent
    }
}
