package com.avawoman.test.injection.modules.framework;

import com.avawoman.test.domain.Repository
import dagger.Module
import dagger.Provides
import org.mockito.Mockito
import javax.inject.Singleton

@Module
internal class MockRepositoryModule{

    @Singleton
    @Provides
    internal fun provideRepository(): Repository = Mockito.mock(Repository::class.java)

}
