package com.avawoman.test.injection.components

import android.app.Application
import com.avawoman.test.DiTest
import com.avawoman.test.injection.modules.compat.ActivityModule
import com.avawoman.test.injection.modules.compat.ApplicationModule
import com.avawoman.test.injection.modules.compat.FragmentModule
import com.avawoman.test.injection.modules.compat.ServiceModule
import com.avawoman.test.injection.modules.framework.MockRepositoryModule
import com.avawoman.test.injection.ApplicationContext
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import dagger.android.support.DaggerApplication
import javax.inject.Singleton

@Singleton
@Component(modules = [
    AndroidSupportInjectionModule::class,
    ApplicationModule::class,
    ActivityModule::class,
    ServiceModule::class,
    FragmentModule::class,
    MockRepositoryModule::class])
interface MockApplicationComponent : AndroidInjector<DaggerApplication> {

    @ApplicationContext
    fun application(): Application

    override fun inject(instance: DaggerApplication)
    fun inject(diTest: DiTest)

    @Component.Builder
    interface Builder {

        @BindsInstance
        @ApplicationContext
        fun application(application: Application): Builder

        fun build(): MockApplicationComponent
    }

}