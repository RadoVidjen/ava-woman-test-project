package com.avawoman.test.base

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.annotation.NonNull
import androidx.annotation.Nullable
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerFragment
import java.net.UnknownHostException
import javax.inject.Inject

abstract class BaseFragment : DaggerFragment(), BaseView {

    @Inject
    lateinit var presenterFactory: ViewModelProvider.Factory

    protected abstract val presenter: ViewModel

    private var savedState: Bundle? = null
    protected lateinit var activity: BaseActivity

    private val fragmentName: String
        get() = this.javaClass.canonicalName

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        activity = (getActivity() as BaseActivity?)!!
    }

    override fun onViewCreated(@NonNull view: View, @Nullable savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        if (savedInstanceState != null && savedState == null) {
            savedState = savedInstanceState.getBundle(fragmentName)
        }
        if (savedState != null) {
            //onStateRestored(savedState!!)
        }
        savedState = null
    }

    override fun onDestroyView() {
        savedState = saveState()
        super.onDestroyView()
    }

    override fun onSaveInstanceState(@NonNull outState: Bundle) {
        super.onSaveInstanceState(outState)
        outState.putBundle(fragmentName, if (savedState != null) savedState else saveState())
    }

    private fun onRestoreInstacneState(savedState: Bundle) {

    }

    private fun saveState(): Bundle? {
        return null
    }

    override fun onNoConnection() {
        activity.onNoConnection()
    }

    override fun onError(throwable: Throwable) {
        when (throwable) {
            is UnknownHostException -> onNoConnection()
            else -> activity.onError(throwable)
        }
    }
}
