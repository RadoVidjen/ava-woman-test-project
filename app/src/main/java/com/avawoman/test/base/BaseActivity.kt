package com.avawoman.test.base

import android.content.DialogInterface
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.widget.Toolbar
import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.avawoman.test.R
import dagger.android.support.DaggerAppCompatActivity
import java.net.UnknownHostException
import javax.inject.Inject

abstract class BaseActivity : DaggerAppCompatActivity(), BaseView {

    @Inject
    lateinit var presenterFactory: ViewModelProvider.Factory

    protected abstract val presenter: ViewModel

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                onBackPressed()
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    protected fun initToolbar(toolbar: Toolbar, shouldShowBack: Boolean) {
        setSupportActionBar(toolbar)
        if (shouldShowBack) {
            supportActionBar?.setDisplayHomeAsUpEnabled(true)
            supportActionBar?.setHomeAsUpIndicator(R.drawable.ic_back)
        } else {
            supportActionBar?.setDisplayHomeAsUpEnabled(false)
        }
    }

    override fun onError(t: Throwable) {
        when (t) {
            is UnknownHostException -> onNoConnection()
        }

    }

    override fun onNoConnection() {
        showAlertDialog()
    }

    private fun showAlertDialog(title: String = getString(R.string.dialog_error_generic_title),
                        message: String = getString(R.string.no_connection_message),
                        action: String = getString(R.string.dialog_error_action_ok),
                        negativeAction: String? = null,
                        dialogAction: DialogInterface.OnClickListener? = null,
                        isCancelable: Boolean = true) {

        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setTitle(title)
        alertDialog.setMessage(message)
        alertDialog.setPositiveButton(action, dialogAction)
        alertDialog.setNegativeButton(negativeAction, dialogAction)
        alertDialog.setCancelable(isCancelable)
        alertDialog.show()
    }

}
