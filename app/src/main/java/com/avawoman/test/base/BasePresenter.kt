package com.avawoman.test.base

import androidx.lifecycle.ViewModel

abstract class BasePresenter<MvpView : BaseView> : ViewModel() {

    protected var mvpView: MvpView? = null

    fun attachView(mvpView: MvpView) {
        this.mvpView = mvpView
    }

    public abstract fun detachView()

    override fun onCleared() {
        detachView()
        this.mvpView = null
        super.onCleared()
    }
}