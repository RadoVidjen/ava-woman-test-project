package com.avawoman.test.base

interface BaseView {

    fun onError(t: Throwable)

    fun onNoConnection()
}
