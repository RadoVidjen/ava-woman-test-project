package com.avawoman.test.util

import android.content.Context
import androidx.core.content.ContextCompat
import com.avawoman.test.R
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade

class CustomCellDecoratorDrop(
        private val date: CalendarDay,
        private val context: Context) : DayViewDecorator {

    override fun shouldDecorate(day: CalendarDay): Boolean {
        return date == day
    }

    override fun decorate(view: DayViewFacade) {
        val drawable = ContextCompat.getDrawable(context, R.drawable.drop)!!
        view.setBackgroundDrawable(drawable)
    }
}