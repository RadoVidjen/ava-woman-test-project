package com.avawoman.test.util

import android.content.Context
import android.graphics.drawable.Drawable
import androidx.core.content.ContextCompat
import com.avawoman.test.R
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator
import com.prolificinteractive.materialcalendarview.DayViewFacade

class CustomCellDecoratorEmpty (
        private val date: CalendarDay,
        private val context: Context) : DayViewDecorator {


    override fun shouldDecorate(day: CalendarDay): Boolean {
        return date == day
    }

    override fun decorate(view: DayViewFacade) {
        val drawable : Drawable = ContextCompat.getDrawable(context, R.color.transparent)!!
        view.setBackgroundDrawable(drawable)
    }
}