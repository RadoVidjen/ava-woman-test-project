package com.avawoman.test.util

import android.content.Context
import androidx.core.content.ContextCompat
import com.avawoman.test.R
import com.prolificinteractive.materialcalendarview.DayViewFacade
import com.prolificinteractive.materialcalendarview.CalendarDay
import com.prolificinteractive.materialcalendarview.DayViewDecorator


class CustomCellDecoratorHeart(
        private val date: CalendarDay,
        private val context: Context) : DayViewDecorator {


    override fun shouldDecorate(day: CalendarDay): Boolean {
        return date == day
    }

    override fun decorate(view: DayViewFacade) {
        view.setBackgroundDrawable(ContextCompat.getDrawable(context, R.drawable.heart)!!)
    }
}