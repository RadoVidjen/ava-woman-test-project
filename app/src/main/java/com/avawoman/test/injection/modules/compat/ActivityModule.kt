package com.avawoman.test.injection.modules.compat;

import com.avawoman.test.base.BaseActivity
import com.avawoman.test.view.main.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module(includes = [PresenterFactoryModule::class])
abstract class ActivityModule {

    @ContributesAndroidInjector()
    abstract fun contributeBaseActivityInjector(): BaseActivity

    @ContributesAndroidInjector()
    abstract fun contributeSplashActivity(): MainActivity

}
