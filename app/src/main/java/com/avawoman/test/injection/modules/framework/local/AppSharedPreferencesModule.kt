package com.avawoman.test.injection.modules.framework.local

import android.app.Application
import android.content.Context
import com.avawoman.test.BuildConfig
import com.avawoman.test.domain.local.AppSharedPreferences
import com.avawoman.test.injection.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
internal class AppSharedPreferencesModule {

    @Singleton
    @Provides
    fun provideSharedPreferences(@ApplicationContext context: Application): AppSharedPreferences =
            AppSharedPreferences(context.getSharedPreferences(BuildConfig.APPLICATION_ID,
                    Context.MODE_PRIVATE))

}