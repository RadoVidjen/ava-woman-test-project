package com.avawoman.test.injection;

import androidx.lifecycle.ViewModel;
import dagger.MapKey;

@MapKey
public @interface ViewModelKey {
    Class<? extends ViewModel> value();
}