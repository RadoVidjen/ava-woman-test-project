package com.avawoman.test.injection.modules.framework;

import com.avawoman.test.injection.modules.framework.local.AppDatabaseModule
import com.avawoman.test.injection.modules.framework.local.AppSharedPreferencesModule
import com.avawoman.test.injection.modules.framework.remote.RemoteServiceModule
import dagger.Module

@Module(includes = [
    AppDatabaseModule::class,
    AppSharedPreferencesModule::class,
    RemoteServiceModule::class])
internal class RepositoryModule
