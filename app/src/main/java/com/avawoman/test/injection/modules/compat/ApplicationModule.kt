package com.avawoman.test.injection.modules.compat

import android.app.Application
import com.avawoman.test.injection.ApplicationContext
import dagger.Binds
import dagger.Module

@Module
abstract class ApplicationModule {

    @Binds
    @ApplicationContext
    internal abstract fun provideApplication(application: Application): Application

}