package com.avawoman.test.injection.modules.compat;

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.avawoman.test.base.PresenterFactory
import com.avawoman.test.injection.ViewModelKey
import com.avawoman.test.view.main.MainPresenter
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class PresenterFactoryModule {

    @Binds
    abstract fun bindViewModelFactory(factory: PresenterFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(MainPresenter::class)
    internal abstract fun bindMainPresenter(main_activity_presenter: MainPresenter): ViewModel

}
