package com.avawoman.test.injection.modules.compat;

import com.avawoman.test.base.BaseFragment
import dagger.Module;
import dagger.android.ContributesAndroidInjector

@Module(includes = [PresenterFactoryModule::class])
abstract class FragmentModule {

    @ContributesAndroidInjector()
    abstract fun contributeBaseFragmentInjector() : BaseFragment;

}
