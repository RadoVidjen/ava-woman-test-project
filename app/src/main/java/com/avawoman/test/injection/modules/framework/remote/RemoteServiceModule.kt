package com.avawoman.test.injection.modules.framework.remote

import com.avawoman.test.BuildConfig
import com.avawoman.test.domain.remote.RemoteService
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.CallAdapter
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton
import com.google.gson.GsonBuilder
import com.google.gson.Gson
import retrofit2.converter.scalars.ScalarsConverterFactory


@Module(includes = [OkHttpModule::class])
internal class RemoteServiceModule {

    @Singleton
    @Named("BASE_URL")
    @Provides
    internal fun provideBaseUrl() = BuildConfig.API_BASE_URL

    fun createGsonWithLenient() : GsonConverterFactory {
        val gson = GsonBuilder()
                .setLenient()
                .create()
        return GsonConverterFactory.create(gson)
    }

    @Singleton
    @Provides
    internal
    fun provideGsonConverterFactory(): GsonConverterFactory = createGsonWithLenient()

    @Singleton
    @Provides
    internal fun provideRxJavaAdapterFactory(): CallAdapter.Factory = RxJava2CallAdapterFactory.create()

    @Singleton
    @Provides
    internal fun provideRemoteService(client: OkHttpClient,
                                      rxConverterFactory: CallAdapter.Factory,
                                      gsonConverterFactory: GsonConverterFactory,
                                      @Named("BASE_URL") baseUrl: String) =
            Retrofit.Builder()
                    .baseUrl(baseUrl)
                    .addCallAdapterFactory(rxConverterFactory)
                    .addConverterFactory(ScalarsConverterFactory.create())
                    .addConverterFactory(gsonConverterFactory)
                    .client(client)
                    .build()
                    .create(RemoteService::class.java)
}