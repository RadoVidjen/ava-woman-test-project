package com.avawoman.test.injection.modules.framework.local

import android.app.Application
import androidx.room.Room
import com.avawoman.test.BuildConfig
import com.avawoman.test.domain.local.AppDatabase
import com.avawoman.test.injection.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Named
import javax.inject.Singleton

@Module
internal class AppDatabaseModule {

    @Singleton
    @Provides
    @Named("DATABASE_NAME")
    fun provideDatabaseName() = BuildConfig.APPLICATION_ID

    @Singleton
    @Provides
    fun provideAppDatabase(@ApplicationContext context: Application,
                           @Named("DATABASE_NAME") databaseName: String): AppDatabase =
            Room.databaseBuilder(context,
                    AppDatabase::class.java, databaseName)
                    .build()


}