package com.avawoman.test.view.main

import android.animation.Animator
import android.os.Bundle
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.avawoman.test.R
import com.avawoman.test.base.BaseActivity
import com.avawoman.test.domain.entity.ResultsResponse
import com.avawoman.test.util.CustomCellDecoratorDrop
import com.avawoman.test.util.CustomCellDecoratorEmpty
import com.avawoman.test.util.CustomCellDecoratorHeart
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : BaseActivity(), MainView {

    override val presenter by lazy {
        val presenter = presenterFactory.create(MainPresenter::class.java)
        presenter.attachView(this)
        presenter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initView()
    }

    override fun onResume() {
        super.onResume()
        presenter.login()
    }

    private fun initView() {
        calendarView.setOnDateChangedListener { _, date, selected ->

            when (selected) {
                true -> {
                    calendarView.setDateSelected(date, true)
                    presenter.selectedDates.add(date)
                }
                false -> {
                    val decorator = CustomCellDecoratorEmpty(date, this@MainActivity)
                    calendarView.setDateSelected(date, false)
                    calendarView.addDecorator(decorator)
                    presenter.selectedDates.remove(date)
                }
            }
        }

        button_sex.setOnClickListener {
            when (presenter.selectedDates.size) {
                0 -> Toast.makeText(this, "Please select date!", Toast.LENGTH_LONG).show()
                else -> {
                    presenter.selectedDates.forEach {
                        val decorator = CustomCellDecoratorHeart(it, this@MainActivity)
                        calendarView.setDateSelected(it, true)
                        calendarView.addDecorator(decorator)
                        presenter.uploadData("sex", presenter.selectedDates)
                    }
                }
            }
        }

        button_period.setOnClickListener {
            when (presenter.selectedDates.size) {
                0 -> Toast.makeText(this, "Please select date!", Toast.LENGTH_LONG).show()
                else -> {
                    presenter.selectedDates.forEach {
                        val decorator = CustomCellDecoratorDrop(it, this@MainActivity)
                        calendarView.setDateSelected(it, true)
                        calendarView.addDecorator(decorator)
                        presenter.uploadData("mens", presenter.selectedDates)
                    }
                }
            }
        }
    }

    override fun showResponse(response: String) {
        tv_server_response.text = response
    }

    override fun showStats(statsResponse: ResultsResponse) {
        animateStats(tv_heart_rate, statsResponse.heartRate.toString())
        animateStats(tv_cycle_number, statsResponse.cycleNumber.toString())
    }

    override fun showProgressBar() {
        progress_bar2.visibility = View.VISIBLE
        progress_bar3.visibility = View.VISIBLE

    }

    override fun hideProgressBar() {
        progress_bar2.visibility = View.GONE
        progress_bar3.visibility = View.GONE
    }


    override fun onDestroy() {
        presenter.detachView()
        super.onDestroy()
    }

    private fun animateStats(view: TextView, stats: String) {
        view.animate()
                .setDuration(400)
                .alpha(0f)
                .setListener(object : Animator.AnimatorListener {
                    override fun onAnimationRepeat(p0: Animator?) = Unit
                    override fun onAnimationCancel(p0: Animator?) = Unit
                    override fun onAnimationStart(p0: Animator?) = Unit
                    override fun onAnimationEnd(p0: Animator?) {
                        view.text = stats
                        view.animate()
                                .setDuration(400)
                                .alpha(1f)
                                .start()
                    }
                })
                .start()
    }
}
