package com.avawoman.test.view.main

import com.avawoman.test.base.BasePresenter
import com.avawoman.test.domain.Repository
import com.prolificinteractive.materialcalendarview.CalendarDay
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class MainPresenter @Inject constructor(private var repository: Repository) : BasePresenter<MainView>() {

    private val disposable: CompositeDisposable = CompositeDisposable()

    var selectedDates: MutableList<CalendarDay> = mutableListOf()

    fun login() {
        mvpView?.showProgressBar()
        repository.getToken()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy(
                        onSuccess = {
                            mvpView?.hideProgressBar()
                            mvpView?.showResponse(it)
                            getStatsPeriodically()
                        },
                        onError = {
                            mvpView?.hideProgressBar()
                            mvpView?.onError(it)
                            it.printStackTrace()
                        }
                ).addTo(disposable)

    }

    private fun getStatsPeriodically() {
        mvpView?.showProgressBar()
        repository.getResults()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy(onNext = {
                    mvpView?.hideProgressBar()
                    mvpView?.showStats(it)
                    mvpView?.showResponse(it.toString())
                }, onError = {
                    mvpView?.hideProgressBar()
                    mvpView?.onError(it)
                    it.printStackTrace()
                }).addTo(disposable)
    }

    fun uploadData(sexOrPeriod: String, dateData: MutableList<CalendarDay>) {
        repository.uploadData(sexOrPeriod, dateData)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribeBy(onSuccess = {
                    selectedDates.clear()
                    mvpView?.showResponse(it.toString())
                    mvpView?.hideProgressBar()
                }, onError = {
                    mvpView?.hideProgressBar()
                    mvpView?.onError(it)
                    it.printStackTrace()
                }).addTo(disposable)

    }


    // Comment test upload
    override fun detachView() {
        disposable.dispose()
    }
}