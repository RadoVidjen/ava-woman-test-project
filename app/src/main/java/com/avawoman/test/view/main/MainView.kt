package com.avawoman.test.view.main

import com.avawoman.test.base.BaseView
import com.avawoman.test.domain.entity.ResultsResponse

interface MainView : BaseView {
    fun showResponse(response: String)
    fun showProgressBar()
    fun hideProgressBar()
    fun showStats(statsResponse: ResultsResponse)
}