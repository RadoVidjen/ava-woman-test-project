package com.avawoman.test.domain.entity

data class Response (var userData: UserData,
                     var serverURL: String,
                     var token: String)