package com.avawoman.test.domain.remote

import com.avawoman.test.domain.entity.DailyActivity
import com.avawoman.test.domain.entity.Response
import com.avawoman.test.domain.entity.ResultsResponse
import io.reactivex.Single
import retrofit2.http.*

interface RemoteService{

    @Headers(
        "X-Auth-Username: TestAndroid",
        "X-Auth-Password: 12345678",
        "Push-Token: n/a",
        "Phone-Type: ANDROID"
    )
    @POST("/server_resolver/login")
    fun getToken() : Single<Response>

    @GET("{URL}/user/results")
    fun getResults(@Path(value = "URL", encoded = true) url: String,
                   @Header("X-Auth-Token") token: String) : Single<ResultsResponse>

    @POST("{URL}/user/boomerang")
    fun uploadData(@Path(value = "URL", encoded = true) url: String,
                   @Header("X-Auth-Token") token: String,
                   @Body requestBody: MutableList<DailyActivity>) : Single<String>

}