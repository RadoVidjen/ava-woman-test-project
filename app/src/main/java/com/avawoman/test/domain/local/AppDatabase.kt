package com.avawoman.test.domain.local;

import androidx.room.Database
import androidx.room.RoomDatabase
import com.avawoman.test.domain.entity.UserData

@Database(entities = [UserData::class], version = 1, exportSchema = false)
abstract class AppDatabase : RoomDatabase()
