package com.avawoman.test.domain.entity

data class DailyActivity(var sex: Boolean,
                         var mens: Boolean,
                         var date: String) {

    override fun toString(): String {
        return "{ \"date\": $date," +
                "\"mens\": $mens, " +
                "\"sex\": $sex" +
                " }"
    }
}

