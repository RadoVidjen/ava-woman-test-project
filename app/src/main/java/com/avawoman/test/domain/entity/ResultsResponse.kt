package com.avawoman.test.domain.entity

data class ResultsResponse(var heartRate: Int,
                           var lastAvaSync: String,
                           var cycleNumber: Int) {

    override fun toString(): String {
        return "{ heartRate: $heartRate, " +
                "lastAvaSync: $lastAvaSync, " +
                "cycleNumber: $cycleNumber" +
                " }"
    }
}
