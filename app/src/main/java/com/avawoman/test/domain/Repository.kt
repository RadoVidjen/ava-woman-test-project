package com.avawoman.test.domain

import com.avawoman.test.domain.entity.BoomerangResponse
import com.avawoman.test.domain.entity.DailyActivity
import com.avawoman.test.domain.entity.ResultsResponse
import com.avawoman.test.domain.local.AppDatabase
import com.avawoman.test.domain.local.AppSharedPreferences
import com.avawoman.test.domain.remote.RemoteService
import com.google.gson.GsonBuilder
import com.prolificinteractive.materialcalendarview.CalendarDay
import io.reactivex.Flowable
import io.reactivex.Single
import java.util.concurrent.TimeUnit
import javax.inject.Inject

const val ACCESS_TOKEN = "access_token"
const val SERVER_URL = "server_url"

class Repository @Inject constructor(
        private val sharedPreferences: AppSharedPreferences,
        private val appDatabase: AppDatabase,
        private val remoteService: RemoteService) {

    /**
     * Simple method that will return token from
     * server if sharedPrefs hasn't already got it.
     * Also saves current server URL.
     */

    fun getToken(): Single<String> {
        return if (sharedPreferences.getSharedPreferencesString(ACCESS_TOKEN, null) != null) {
            Single.create {
                val token = sharedPreferences.getSharedPreferencesString(ACCESS_TOKEN, null)
                it.onSuccess(token!!)
            }
        } else {
            remoteService
                    .getToken()
                    .map {
                        sharedPreferences.putSharedPreferencesString(ACCESS_TOKEN, it.token)
                        sharedPreferences.putSharedPreferencesString(SERVER_URL, it.serverURL)
                        it.token
                    }
        }
    }

    /**
     *  Method that will periodically update UI with
     *  responses from server.
     */

    fun getResults(): Flowable<ResultsResponse> {
        val token = sharedPreferences.getSharedPreferencesString(ACCESS_TOKEN, null)!!
        val serverUrl = sharedPreferences.getSharedPreferencesString(SERVER_URL, null)!!
        return remoteService
                .getResults(url = serverUrl, token = token)
                .repeatWhen { it.delay(10, TimeUnit.SECONDS) }
    }

    /**
     * Upload selected dates.
     */

    fun uploadData(sexOrPeriod: String, dateData: MutableList<CalendarDay>): Single<BoomerangResponse> {
        val requestBody = mutableListOf<DailyActivity>()
        val token = sharedPreferences.getSharedPreferencesString(ACCESS_TOKEN, null)!!
        val serverUrl = sharedPreferences.getSharedPreferencesString(SERVER_URL, null)!!

        dateData.forEach {
            val formattedDate = "\"${it.year}-${it.month}-${it.day}\""
            val dailyActivity: DailyActivity = when (sexOrPeriod) {
                "mens" -> DailyActivity(mens = true, sex = false, date =  formattedDate)
                else -> DailyActivity(mens = false, sex = true, date = formattedDate)
            }
            requestBody.add(dailyActivity)
        }

        return remoteService.uploadData(url = serverUrl,
                token = token,
                requestBody = requestBody)
                .map {
                    GsonBuilder().create().fromJson(fixMalformedJson(it), BoomerangResponse::class.java)
                }
    }

    /**
     * Method will replace malformed json response with the correct one.
     */
    private fun fixMalformedJson(malformedJson: String): String {
        val fixedString = malformedJson.replaceFirst("boomerang", "\"boomerang\"")
        return "{$fixedString}"
    }

}