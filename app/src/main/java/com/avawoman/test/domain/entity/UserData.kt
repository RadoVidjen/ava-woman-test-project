package com.avawoman.test.domain.entity

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity
data class UserData(var firstName: String = "",
                    var lastName: String = "",
                    var country: String = "",
                    var csAccess: Boolean,
                    @PrimaryKey var email: String = "",
                    var username: String = "")








