package com.avawoman.test.domain.entity

data class BoomerangResponse(var boomerang: MutableList<DailyActivity>) {

    override fun toString(): String {
        return when(boomerang.size) {
            1-> "{ \"boomerang\" : ${boomerang[0]} }"
            else -> "{ \"boomerang\" : $boomerang }"
        }
    }
}