package com.avawoman.test.domain.local;

import android.annotation.SuppressLint
import android.content.SharedPreferences

class AppSharedPreferences constructor(private var sharedPreferences: SharedPreferences) {

    fun putSharedPreferencesInt(key: String, value: Int) {
        val edit = sharedPreferences.edit()
        edit.putInt(key, value)
        edit.apply()
    }

    fun putSharedPreferencesBoolean(key: String, `val`: Boolean) {
        val edit = sharedPreferences.edit()
        edit.putBoolean(key, `val`)
        edit.apply()
    }

    fun putSharedPreferencesString(key: String, `val`: String?) {
        val edit = sharedPreferences.edit()
        edit.putString(key, `val`)
        edit.apply()
    }

    fun putSharedPreferencesStringSet(key: String, `val`: Set<String>) {
        val edit = sharedPreferences.edit()
        edit.putStringSet(key, `val`)
        edit.apply()
    }

    fun putSharedPreferencesFloat(key: String, `val`: Float) {
        val edit = sharedPreferences.edit()
        edit.putFloat(key, `val`)
        edit.apply()
    }

    fun putSharedPreferencesLong(key: String, `val`: Long) {
        val edit = sharedPreferences.edit()
        edit.putLong(key, `val`)
        edit.apply()
    }

    fun getSharedPreferencesLong(key: String, _default: Long): Long {
        return sharedPreferences.getLong(key, _default)
    }

    fun getSharedPreferencesFloat(key: String, _default: Float): Float {
        return sharedPreferences.getFloat(key, _default)
    }

    fun getSharedPreferencesString(key: String, _default: String?): String? {
        return sharedPreferences.getString(key, _default)
    }

    fun getSharedPreferencesStringSet(key: String): Set<String>? {
        return sharedPreferences.getStringSet(key, null)
    }

    fun getSharedPreferencesInt(key: String, _default: Int): Int {
        return sharedPreferences.getInt(key, _default)
    }

    fun getSharedPreferencesBoolean(key: String, _default: Boolean): Boolean {
        return sharedPreferences.getBoolean(key, _default)
    }

    fun clearKey(key: String) {
        sharedPreferences.edit().remove(key).apply()
    }

    @SuppressLint("ApplySharedPref")
    fun clear() {
        sharedPreferences.edit().clear().commit()
    }

}