package com.avawoman.test

import androidx.multidex.MultiDex
import com.avawoman.test.injection.components.DaggerApplicationComponent
import dagger.android.AndroidInjector
import dagger.android.support.DaggerApplication
import io.reactivex.plugins.RxJavaPlugins
import timber.log.Timber

open class App : DaggerApplication() {

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerApplicationComponent.builder()
                .application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        MultiDex.install(this)

        RxJavaPlugins.setErrorHandler { e ->
            Timber.e(e)
        }
    }

}